REM build_all.cmd
REM
REM By Juan Menendez <kohbosan@gmail.com>
REM Copyright 2017 Juan Menendez
REM
REM This program is free software; you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation; either version 2 of the License, or
REM (at your option) any later version.
REM
REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along
REM with this program; if not, write to the Free Software Foundation, Inc.,
REM 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@echo off
set myPath=%~dp0

IF [%WIRESHARK_BASE_DIR%] == [] (
  REM
  REM change this if you change version
  REM
  set WIRESHARK_BASE_DIR=D:\Documents\Coding\wireshark

  REM
  REM Note if you want to change this AFTER you have built:
  REM Do 'clean', then 'prep', then 'build', 'package'
  REM
  set WIRESHARK_VERSION_EXTRA=-datasync

  set CYGWIN=nodosfilewarning
  set WIRESHARK_TARGET_PLATFORM=win64
  set QT5_BASE_DIR=C:\Dev\Qt\5.9.2\msvc2015_64

  REM set WIRESHARK_CYGWIN_INSTALL_PATH=c:\cygwin or whatever other path that is applicable to your setup
  set WIRESHARK_CYGWIN_INSTALL_PATH=C:\Dev\Cygwin
)


IF [%myPath%] == [%cd%\] (
  echo Do not run this command from the source directory
  echo "(run it from the target output directory)"
  echo i.e. ..\wireshark\build_all.cmd %*
  exit /b
)

IF [%1] == [build] (
  msbuild /m /p:Configuration=RelWithDebInfo Wireshark.sln
  exit /b
)

IF [%1] == [clean] (
  msbuild /m /p:Configuration=RelWithDebInfo Wireshark.sln /t:Clean
  exit /b
)

IF [%1] == [package] (
  msbuild /m /p:Configuration=RelWithDebInfo nsis_package_prep.vcxproj
  msbuild /m /p:Configuration=RelWithDebInfo nsis_package.vcxproj
  exit /b
)

IF [%1] == [prep] (
  @echo cmake -Wno-dev -DENABLE_CHM_GUIDES=on -G "Visual Studio 14 2015 Win64" %myPath%\..\..\
  cmake -Wno-dev -DENABLE_CHM_GUIDES=on -G "Visual Studio 14 2015 Win64" %myPath%\..\..\
  exit /b
)

IF [%1] == [rebuild] (
    call %myPath%build_all.cmd clean
    call %myPath%build_all.cmd prep
    call %myPath%build_all.cmd build
    exit /b
)

echo %myPath%build_all.cmd {prep^|build^|clean^|package}
exit /b