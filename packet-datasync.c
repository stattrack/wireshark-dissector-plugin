/* packet-datasync.c
 *
 * By Juan Menendez <kohbosan@gmail.com>
 * Copyright 2017 Juan Menendez
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <string.h>
#include <packet-datasync.h>

void proto_register_mdsp(void);
void proto_reg_handoff_mdsp(void);

static int proto_mdsp = -1;
static int hf_mdsp_type = -1;
static int hf_mdsp_msg = -1;
static gint ett_mdsp = -1;

static const value_string typenames[] = {
    {1, "Load"},
    {15, "Clear"},
    {0, NULL}
};

static int
dissect_mdsp(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree _U_, void *data _U_)
{
    gint offset = 0;

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "MDSP");
    /* Clear out stuff in the info column */
    col_clear(pinfo->cinfo, COL_INFO);

    proto_item *ti = proto_tree_add_item(tree, proto_mdsp, tvb, 0, -1, ENC_NA);
    proto_tree *mdsp_tree = proto_item_add_subtree(ti, ett_mdsp);
    proto_tree_add_item(mdsp_tree, hf_mdsp_type, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(mdsp_tree, hf_mdsp_msg, tvb, offset, -1, ENC_BIG_ENDIAN);

    return tvb_captured_length(tvb);
}

void 
proto_register_mdsp(void)
{
    static hf_register_info hf[] = {
        {
            &hf_mdsp_type,
            {
                "Type",
                "mdsp.type",
                FT_UINT8,
                BASE_HEX,
                VALS(typenames),
                0x0,
                NULL,
                HFILL
            }
        },
        {
            &hf_mdsp_msg,
            {
                "Message",
                "mdsp.msg",
                FT_STRING,
                BASE_NONE,
                NULL,
                0x0,
                NULL,
                HFILL
            }
        }
    };

    static gint *ett[] = {
        &ett_mdsp
    };

    proto_mdsp = proto_register_protocol(
        "Mobile Data Synch Protocol", /* name       */
        "mobdatasync",                /* short name */
        "mdsp"                        /* abbrev     */
        );
    
    proto_register_field_array(proto_mdsp, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
}

void 
proto_reg_handoff_mdsp(void)
{
    static dissector_handle_t mdsp_handle;

    mdsp_handle = create_dissector_handle(dissect_mdsp, proto_mdsp);
    dissector_add_uint("udp.port", MDSP_PORT, mdsp_handle);
}